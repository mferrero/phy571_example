# Project description

The 2D [Ising Model](https://en.wikipedia.org/wiki/Ising_model) is
simulated using a Monte Carlo method. Physical observables, such as
the average energy and specific heat, are computed from the
simulation.

# Usage

To compute the physical observables of the system, go to the
production directory and run

```python
python3 compute_observables.py
```

The result of this script are stored in results. To generate a plot of
the data run the jupyter notebook called
`analysis/analyze_observables.ipynb`. The resulting figure is placed
in the figures directory.

# Directory structure
* design
  * contains a notebook that was used to organize the main pieces of
    the simulation
* production
  * ising2d.py contains the main Monte Carlo simulation of the 2D
    Ising Model
  * compute_observables.py uses the library ising2d.py to compute
    physical observables
* results
  * the computed observables are stored in files
* analysis
  * contains a notebook that reads in the results and plots them
* figures
  * contains the figures for the report
* report
  * contains the project report
* extras
  * example of reading command-line arguments
  * example of reading input simulation values from a file