"""Short example of how to use the python library argparse to get
values from the command line.  See
https://docs.python.org/3/howto/argparse.html for more details.

Parsing command-line arguments is very useful for running
simulations. Imagine that you want to run many simulations where you
change a few of the input values. Instead of changing these values in
the python file directly, we can pass the values that change on the
command-line. For example

> python3 command_line_arguments.py num_points=10 grid_spacing=0.1

It even prints a helpful message that reminds you how to use your
program

> python3 command_line_arguments.py -h

"""

import argparse

# initialize the parser, add a program description
parser = argparse.ArgumentParser(description='My cool simulation')

# add required arguments (the program gives an error if it doesn't
# receive these values)
parser.add_argument('num_points', type=int,
                    help='Number of points in computational grid')
parser.add_argument('grid_spacing', type=float,
                    help='The spacing between points of the grid')

# add an optional argument
parser.add_argument('--output_file', help='Optional filename to write values to')

# parse the command-line arguments
args = parser.parse_args()

# do something with the arguments, here we just print the required arguments
print('Received command-line arguments:')
print('num_points = {}'.format(args.num_points))
print('grid_spacing = {}'.format(args.grid_spacing))

# if an output filename was given, then write the required arguments
# to that filename
if args.output_file:
    with open(args.output_file, 'w') as f:
        f.write('num_points = {}\n'.format(args.num_points))
        f.write('grid_spacing = {}\n'.format(args.grid_spacing))
    print('Wrote data to {}'.format(args.output_file))
