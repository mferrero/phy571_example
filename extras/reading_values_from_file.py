"""One possible way to store simulation parameters in a file is to use
the 'conf' format, which stands for configuration.

This file is structured like this:
[section 1]
key = value

[section 2]
a = 1000

Below are two simple functions to help extract the values from a conf file.

"""

from os.path import join, abspath, dirname, basename, isfile
import configparser

def load_config(filename):
    """Load the parameters from a configuration file that has the syntax
[section]
key = value

and return them as a hierarchical dictionary in the form
{'section': {'key': value}}

"""
    if not isfile(filename):
        raise OSError('Config file not found: {}'.format(filename))
    
    cfg = configparser.ConfigParser()
    cfg.optionxform = str # don't change capitalization
    cfg.read(filename)
    
    # convert configparser object into standard python dictionary
    d = {s:{} for s in cfg.sections()}
    
    for section in cfg.sections():
        for key in cfg[section]:
            d[section][key] = convert_type(cfg[section][key])

    return d


def convert_type(string):
    """Attempts to convert string into an int or float. If possible
returns the converted value, otherwise returns the original string.

    """
    try:
        value = int(string)
        return value
    except ValueError:
        pass

    try:
        value = float(string)
        return value
    except ValueError:
        return string



if __name__ == '__main__':
    data = load_config('input_values')
    print(data)
