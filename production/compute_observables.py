#
# compute_observables: A production script that computes:
# 
# - the average energy
# - the average magnetization
# - the specific heat
# - the magnetic susceptibility
#
# The results are saved in text files in ../results/
#

import ising2d
import numpy as np
from mpi4py import MPI

# communicator and local rank
comm = MPI.COMM_WORLD
rank = comm.Get_rank()

# parameters
L = 4
n_cycles = 100000
length_cycle = L*L
n_warmup = 1000

# temperature range
temp_range = np.hstack([np.arange(0.5,2.,0.5), np.arange(2.,2.5,0.05), np.arange(2.5,5,0.5)])

# create containers for observables
mag = np.zeros_like(temp_range)
energ = np.zeros_like(temp_range)
chi = np.zeros_like(temp_range)
cv = np.zeros_like(temp_range)

# loop over temperatures
for i, T in enumerate(temp_range):

    config = ising2d.Configuration(T, 1.0, L)
    
    av_m, av_m2, av_e, av_e2, n_tot = 0,0,0,0,0

    # Monte Carlo
    for n in range(n_warmup+n_cycles):
        
        for k in range(length_cycle):
            ising2d.metropolis_move(config)
        
        if n >= n_warmup:
            av_e  += config.energy
            av_e2 += config.energy**2
            av_m  += config.magnetization
            av_m2 += config.magnetization**2
            n_tot  += 1
            
    # reduce from all cores
    comm.allreduce(av_e)
    comm.allreduce(av_e2)
    comm.allreduce(av_m)
    comm.allreduce(av_m2)
    comm.allreduce(n_tot)

    # normalize averages
    av_m  /= float(n_tot)
    av_m2 /= float(n_tot)
    av_e  /= float(n_tot)
    av_e2 /= float(n_tot)
            
    # get physical quantities
    fact = 1./L**2
    mag[i] = fact * av_m
    energ[i] = fact * av_e
    cv[i] = fact * (av_e2 - av_e**2) / T**2
    chi[i] = fact * (av_m2 - av_m**2) / T
    
    # print info because progress can be slow
    if rank == 0: print("T = %f and %.2f percent done"%(T, (100.*(i+1))/len(temp_range)))

# save quantities in a file
if rank == 0:
    np.savetxt("../results/energ_%i.dat"%L, energ)
    np.savetxt("../results/mag_%i.dat"%L, mag)
    np.savetxt("../results/cv_%i.dat"%L, cv)
    np.savetxt("../results/chi_%i.dat"%L, chi)
