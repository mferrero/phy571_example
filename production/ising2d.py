#
# ising2d: A library for the two-dimensional Ising model
# 
# The Ising model is described by the classical Hamiltonian
# 
# \begin{equation}
#   \mathcal{H} = -J \sum_{\langle i,j \rangle} \sigma_i \sigma_j
# \end{equation}
# 
# where $\sigma_i=\pm1$ is an Ising spin on the site $i$ of a square
# lattice and $J>0$ is a ferromagnetic coupling between nearest-neighbor
# spins. The square lattice has $N = L \times L$ sites.
# 

import numpy as np
import numpy.random as rnd
import itertools


class Configuration(object):
    """A configuration of Ising spins"""

    def __init__(self, T, J, L):
        """Construct a configuration at temperature T, with coupling J and size L"""
        self.size = L
        self.J = J
        self.beta = 1./T
        self.spins = rnd.choice([-1,1], size=(L,L))      # either +1 or -1
        self.energy = self._get_energy()
        self.magnetization = self._get_magnetization()
        
    def _get_energy(self):
        """Returns the total energy"""
        energ = 0.
        for i,j in itertools.product(range(self.size), repeat=2):
            energ += -self.J * self.spins[i,j] * (self.spins[i,(j+1)%self.size] + self.spins[(i+1)%self.size,j])
        return energ
    
    def _get_magnetization(self):
        """Returns the total magnetization"""
        magnet = np.sum(self.spins)
        return magnet        


def config_to_image(config):
    """Turn an array into an image"""
    L = config.size
    im = np.zeros([L,L,3])
    for i,j in itertools.product(range(L), repeat=2):
        im[i,j,:] = (1,0,0) if config.spins[i,j]==1 else (0,0,0)
    return im


def metropolis_move(config):
    """Modify (or not) a configuration with Metropolis algorithm"""
    
    L = config.size
    J = config.J
    beta = config.beta
    i, j = rnd.randint(L, size=(2)) # pick a random site
    
    # compute energy difference
    coef = 2 * J * config.spins[i,j]
    delta_energy = coef * (config.spins[i,(j+1)%L] + config.spins[(i+1)%L,j] + config.spins[i,(j-1)%L] + config.spins[(i-1)%L,j])
    
    # accept modification with Metropolis probability
    # if not accepted: leave configuration unchanged
    if rnd.random() < np.exp(-beta*delta_energy):
        config.spins[i,j] *= -1
        config.energy += delta_energy
        config.magnetization += 2*config.spins[i,j]
